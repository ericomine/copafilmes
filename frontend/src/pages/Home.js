import React from 'react';
import Container from 'react-bootstrap/Container';
import MovieForm from '../components/MovieForm';
import '../styles/Home.css';
import InfoSection from '../components/InfoSection';

export default class Home extends React.Component {
  render() {
    return (
      <Container className="cont">
      <InfoSection page={"home"}></InfoSection>
      <MovieForm></MovieForm>
      </Container>
    );
  }
}