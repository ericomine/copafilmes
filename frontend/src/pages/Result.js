import React from 'react';
import Container from 'react-bootstrap/Container';
import Winners from '../components/Winners';
import InfoSection from '../components/InfoSection';

export default class Home extends React.Component {
  render() {
    return (
      <Container className="cont">
        <InfoSection page={"result"}></InfoSection>
        <Winners resultUri={this.props.location.state.resultUri} ></Winners>
      </Container>
    );
  }
}