/* React */
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
/* Bootstrap */
import 'bootstrap/dist/css/bootstrap.min.css';
/* Project */
import Home from './pages/Home';
import Result from './pages/Result';

class App extends React.Component {
  render() {
    return(
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/result" component={Result} />
        </Switch>
      </Router>      
    );
  }
}

export default App;