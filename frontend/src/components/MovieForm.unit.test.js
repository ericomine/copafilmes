import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import MovieForm from './MovieForm';

 
Enzyme.configure({ adapter: new Adapter() });

test('verifies that MovieForm loadad movies from API', async () => {
  const component = mount(<MovieForm />);
  await component.instance().componentDidMount();
  expect(component.instance().state.movies).toHaveLength(16);
});

test('verifies that clicking a card increments counter', async () => {
  const component = mount(<MovieForm />);
  await component.instance().componentDidMount();
  await component.setProps({}); // This is called to force re-rendering.

  expect(component.instance().state.countSelected).toEqual(0);
  
  let count = 0;
  component.find('Card').forEach((node) => {
    expect(component.instance().state.countSelected).toEqual(count);
    node.prop('onClick')();
    count += 1;
    expect(component.instance().state.countSelected).toEqual(count);
    
    // Please uncomment if you want to double check this test.
    // console.log(component.instance().state.countSelected);
  })
});

test('verifies that button is only clickable when counter === 8', async () => {
    const component = mount(<MovieForm />);
    await component.instance().componentDidMount();
    await component.setProps({}); // This is called to force re-rendering.
  
    expect(component.instance().state.countSelected).toEqual(0);
    
    component.find('Card').forEach(async (node) => {
      const btnDisabled = component.find('Button').props().disabled;
      const countSelected = component.instance().state.countSelected;

      expect(btnDisabled).toEqual(countSelected !== 8);
      
      node.prop('onClick')();
      component.setProps({}); // This is called to force re-rendering.
    })
});