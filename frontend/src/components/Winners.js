import React from 'react';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import '../styles/Winners.css'
import fetchHelpers from './FetchHelpers';


export default class Winners extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first : {
        id : '',
        title : '',
        year: 0,
        rating: 0.0
      },
      second : {
        id : '',
        title : '',
        year: 0,
        rating: 0.0
      }
    }
  }
  
  async componentDidMount() {
    const results = await fetchHelpers.getResults(this.props.resultUri);

    this.setState({
      first: results.first,
      second: results.second
    });
  }

  render() {
    return(
      <Container className="winners-container">
        <Row className="winner-row">
          <Col className="winner-position"><h2>1º</h2></Col>
          <Col className="winner-title"><h2>{this.state.first.title}</h2></Col>
        </Row>
        <Row className="winner-row">
          <Col className="winner-position"><h2>2º</h2></Col>
          <Col className="winner-title"><h2>{this.state.second.title}</h2></Col>
        </Row>
      </Container>
    );
  }
}