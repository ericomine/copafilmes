import React from 'react';
import '../styles/MovieCard.css';

export default class MovieCard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      _title: props.title,
      _year: props.year,
      _selected: props.selected,
      _ySelColor: 'springgreen',
      _nSelColor: 'aliceblue'
    }
  }

  componentDidUpdate() {
    if (this.props.selected !== this.state._selected) {
      this.setState({_selected : this.props.selected});
    }   
  }

  render() {
    return (
      <div className='moviecard-div'
        style={{ backgroundColor:
          this.state._selected === true ?
            this.state._ySelColor :
            this.state._nSelColor}}>
        <span>{this.state._year}</span>  
        <h3>{this.state._title}</h3>
      </div>
    )
  }

}