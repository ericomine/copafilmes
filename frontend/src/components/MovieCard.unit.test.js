import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import MovieCard from './MovieCard';
 
Enzyme.configure({ adapter: new Adapter() });

test('verifies that state holds correct _selected value', () => {
  const wrapper = shallow(<MovieCard title="title" year="2020" selected={true} ></MovieCard>);
  const stateSelected = wrapper.instance().state._selected;
  expect(stateSelected).toEqual(true);
});