import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import '../styles/Home.css';

export default class InfoSection extends React.Component {
  render() {
    if (this.props.page === "home") {
      return (
        <Jumbotron className="jumbo">
          <div>
            <h5>Campeonato de Filmes</h5>
            <h2>Fase de Seleção</h2>
            <span>Selecione 8 filmes que você deseja que entrem na competição e 
              depois pressione o botão Gerar Meu Campeonato para Prosseguir</span>
            </ div>
        </Jumbotron>
      );
    } else if (this.props.page === "result") {
      return (
        <Jumbotron className="jumbo">
          <div>
            <h5>Campeonato de Filmes</h5>
            <h2>Resultado Final</h2>
            <span>Veja o resultado final de forma simples e rápida.</span>
          </ div>
        </Jumbotron>
      );
    }
  }
}