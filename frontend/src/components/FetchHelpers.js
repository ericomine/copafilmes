import fetch from 'node-fetch';

const fetchHelpers = {
  getMoviesAsArray: async function() {
    const response = await fetch('http://copafilmes.azurewebsites.net/api/filmes')
      .then(results => results.json())
      .then(results => results.map((v,k)=>({...v, selected: false})))
      .catch(err => console.log(err));

    return response;
  },

  formatMoviesAsJson: function(movies) {
    const result = JSON.stringify(
      movies.filter((m) => m.selected)
        .map((m) => (
          { "id" : m.id,
            "title" : m.titulo,
            "year" : m.ano,
            "rating" : m.nota }
    )));

    return result;
  },

  postMovies: async function(movies) {
    const myBody = this.formatMoviesAsJson(movies);
    const response = await fetch('http://localhost:5000/MovieCup', {
      method: 'POST',
      headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json; charset=UTF-8'
      },
      body: myBody
      })
    .then(response => response.json())
    .catch(err => console.log(err));
    
    const redirectTo =
      (response.headers[0].Key === 'Location') ?
        response.headers[0].Value[0] :
        null;

    return redirectTo;
  },

  getResults: async function(resultUri) {
    const response = await fetch(resultUri)
      .then(results => results.json())
      .catch(err => console.log(err));
    
    const first = {...response}[0].first;
    const second = {...response}[0].second;

    return { first, second };
  },
}

export default fetchHelpers;