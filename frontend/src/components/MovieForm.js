import React from 'react';
import { Redirect } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import CardDeck from 'react-bootstrap/CardDeck';
import Card from 'react-bootstrap/Card';
import MovieCard from './MovieCard';
import fetchHelpers from './FetchHelpers';
import '../styles/MovieCard.css';


export default class MovieForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: [],
      countSelected : 0,
      resultOk: false,
      resultUri: null
    }
  }
  
  async componentDidMount() {
    const result = await fetchHelpers.getMoviesAsArray();
    this.setState({movies: result});

    console.log(this.state.movies);
  }

  async clickSend() {
    const location = await fetchHelpers.postMovies(this.state.movies);
    this.setState({resultOk : true, resultUri : location})
  }

  clickSelectMovie(key) {
    const aux = this.state.movies
      .map((v, k) => (k === key ? { ...v, selected: !v.selected } : v
    ));

    this.setState({
      movies : aux,
      selectedIds : aux.filter((m) => m.selected).map((v, k) => v.id),
      countSelected : aux.filter((m) => m.selected).length
      });
  }


  render() {
    if (this.state.resultOk) {
      return <Redirect to={{
        pathname: '/result',
        state: { resultUri: this.state.resultUri }
      }} />
    }

    return(
      <Container>
        <Row><Col>
          <h5 className='float-left'>
            Selecionados: {this.state.countSelected} / 8
          </h5>
          <Button
            id='sendbutton'
            className='float-right'
            disabled={this.state.countSelected !== 8}
            onClick={() => this.clickSend()}>
              Gerar meu campeonato
          </Button>
          </Col></Row>
        <Row>
          <CardDeck>
          {this.state.movies.map((item, index) => {
            return (
            <Card key={index}
              id='moviecard'
              className='moviecard'
              onClick={() => this.clickSelectMovie(index)}>
              <MovieCard key={index}
                title={item.titulo}
                year={item.ano}
                selected={item.selected} />
            </Card> );
          })}
          </CardDeck>
        </Row>
      </Container>
    );
  }
}