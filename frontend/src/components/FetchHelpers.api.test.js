import fetchHelpers from './FetchHelpers'
import fetchMocks from './fetchMocks'

test("get movies should match mock match", async() => {
  const realData = await fetchHelpers.getMoviesAsArray();
  expect(realData).toHaveLength(16);
  for (var i = 0; i < 16; i++) {
    expect(realData[i]).toHaveProperty('id');
    expect(realData[i]).toHaveProperty('titulo');
    expect(realData[i]).toHaveProperty('ano');
    expect(realData[i]).toHaveProperty('nota');
  }
});

test("format movies as json should follow format", () => {
  const mockResult = fetchMocks.formatMoviesAsJsonMock();
  const mockInput = fetchMocks.getMoviesAsArray();
  const funcResult = fetchHelpers.formatMoviesAsJson(mockInput);
  expect(funcResult).toEqual(mockResult);
})

test("get result should match mock match, requires real backend response",
  async() => {
    const mockData = fetchMocks.getMoviesAsArray();
    const resultUri = await fetchHelpers.postMovies(mockData);
    expect(resultUri).toMatch('/MovieCup/');
});

test("get result should have fields below, requires real backend response",
  async() => {
    const mockData = fetchMocks.getMoviesAsArray();
    const resultUri = await fetchHelpers.postMovies(mockData);
    const result = await fetchHelpers.getResults(resultUri);

    expect(result).toHaveProperty('first');
    expect(result).toHaveProperty('first.id');
    expect(result).toHaveProperty('first.title');
    expect(result).toHaveProperty('first.year');
    expect(result).toHaveProperty('first.rating');
    expect(result).toHaveProperty('second');
    expect(result).toHaveProperty('second.id');
    expect(result).toHaveProperty('second.title');
    expect(result).toHaveProperty('second.year');
    expect(result).toHaveProperty('second.rating');
});