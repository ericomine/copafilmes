const fetchMocks = {

  getMoviesAsArray: function() {
    const movies = [
      {id: "tt3606756", titulo: "Os Incríveis 2", ano: 2018, nota: 8.5, selected: true},
      {id: "tt4881806", titulo: "Jurassic World: Reino Ameaçado", ano: 2018, nota: 6.7, selected: true},
      {id: "tt5164214", titulo: "Oito Mulheres e um Segredo", ano: 2018, nota: 6.3, selected: true},
      {id: "tt7784604", titulo: "Hereditário", ano: 2018, nota: 7.8, selected: true},
      {id: "tt4154756", titulo: "Vingadores: Guerra Infinita", ano: 2018, nota: 8.8, selected: true},
      {id: "tt5463162", titulo: "Deadpool 2", ano: 2018, nota: 8.1, selected: true},
      {id: "tt3778644", titulo: "Han Solo: Uma História Star Wars", ano: 2018, nota: 7.2, selected: true},
      {id: "tt3501632", titulo: "Thor: Ragnarok", ano: 2017, nota: 7.9, selected: true},
      {id: "tt2854926", titulo: "Te Peguei!", ano: 2018, nota: 7.1, selected: false},
      {id: "tt0317705", titulo: "Os Incríveis", ano: 2004, nota: 8, selected: false},
      {id: "tt3799232", titulo: "A Barraca do Beijo", ano: 2018, nota: 6.4, selected: false},
      {id: "tt1365519", titulo: "Tomb Raider: A Origem", ano: 2018, nota: 6.5, selected: false},
      {id: "tt1825683", titulo: "Pantera Negra", ano: 2018, nota: 7.5, selected: false},
      {id: "tt5834262", titulo: "Hotel Artemis", ano: 2018, nota: 6.3, selected: false},
      {id: "tt7690670", titulo: "Superfly", ano: 2018, nota: 5.1, selected: false},
      {id: "tt6499752", titulo: "Upgrade", ano: 2018, nota: 7.8, selected: false},
    ]
    return movies;
  },

  formatMoviesAsJsonMock: function() {
    const jsonString = '[{"id":"tt3606756","title":"Os Incríveis 2","year":2018,"rating":8.5},{"id":"tt4881806","title":"Jurassic World: Reino Ameaçado","year":2018,"rating":6.7},{"id":"tt5164214","title":"Oito Mulheres e um Segredo","year":2018,"rating":6.3},{"id":"tt7784604","title":"Hereditário","year":2018,"rating":7.8},{"id":"tt4154756","title":"Vingadores: Guerra Infinita","year":2018,"rating":8.8},{"id":"tt5463162","title":"Deadpool 2","year":2018,"rating":8.1},{"id":"tt3778644","title":"Han Solo: Uma História Star Wars","year":2018,"rating":7.2},{"id":"tt3501632","title":"Thor: Ragnarok","year":2017,"rating":7.9}]'
    return jsonString;
  },

  postMovies: function(body) {
    return null;
  },
  
  getResults: function() {
    const result = {
      first: {
        id: "tt4154756",
        title: "Vingadores: Guerra Infinita",
        year: 2018,
        rating: 8.8},
      second: {
        id: "tt3606756",
        title: "Os Incríveis 2",
        year: 2018,
        rating: 8.5}
      }
    return result;
  },
}

export default fetchMocks;