import React from 'react';
import InfoSection from './InfoSection';

import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
 
Enzyme.configure({ adapter: new Adapter() });

test('info section for home renders correctly', () => {
  const wrapper = shallow(<InfoSection page="home"></InfoSection>);
  expect(wrapper).toMatchSnapshot();
});

test('info section for result renders correctly', () => {
  const wrapper = shallow(<InfoSection page="result"></InfoSection>);
  expect(wrapper).toMatchSnapshot();
});