﻿using CopaFilmesAPI.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace CopaFilmesAPI.Tests
{
    public static class Mocks
    {
        public static List<Movie> GetTestSet()
        {
            var movies = new List<Movie>();
            movies.Add(new Movie { Id = "tt3606756", Title = "Os Incríveis 2", Year = 2018, Rating = 8.5 } );
            movies.Add(new Movie { Id = "tt4881806", Title = "Jurassic World: Reino Ameaçado", Year = 2018, Rating = 6.7 });
            movies.Add(new Movie { Id = "tt5164214", Title = "Oito Mulheres e um Segredo", Year = 2018, Rating = 6.3 });
            movies.Add(new Movie { Id = "tt7784604", Title = "Hereditário", Year = 2018, Rating = 7.8 });
            movies.Add(new Movie { Id = "tt4154756", Title = "Vingadores: Guerra Infinit", Year = 2018, Rating = 8.8 });
            movies.Add(new Movie { Id = "tt5463162", Title = "Deadpool 2", Year = 2018, Rating = 8.1 });
            movies.Add(new Movie { Id = "tt3778644", Title = "Han Solo: Uma História Star Wars", Year = 2018, Rating = 7.2 });
            movies.Add(new Movie { Id = "tt3501632", Title = "Thor: Ragnarok", Year = 2017, Rating = 7.9 });
            return movies;
        }
        
    }
}
