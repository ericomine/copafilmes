using System;
using Xunit;
using CopaFilmesAPI.Domain;
using System.Collections.Generic;
using CopaFilmesAPI.Services;
using CopaFilmesAPI.Data;

namespace CopaFilmesAPI.Tests
{
    public class IntegrationTests
    {
        [Fact]
        public void TestCupProcessor()
        {
            CupProcessor cup = new CupProcessor(Mocks.GetTestSet());
            Assert.True(cup.FirstPlace.Id == "tt4154756");
            Assert.True(cup.SecondPlace.Id == "tt3606756");
        }
    }
}
