﻿using Xunit;
using CopaFilmesAPI.Domain;
using System.Text.RegularExpressions;
using CopaFilmesAPI.Data;
using System.Linq;

namespace CopaFilmesAPI.Tests
{
    public class UnitTests
    {
        private bool IsValidId(string movieId)
        {
            Regex regex = new Regex(@"^t{2}\d{7}$");
            return regex.IsMatch(movieId);
        }

        [Theory]
        [InlineData("tt4154756", true)]
        [InlineData("t4154756", false)]
        [InlineData("4154756", false)]
        [InlineData("4154756tt", false)]
        public void CheckIdFormatTheory(string movieId, bool expectedResult)
        {
            Assert.Equal(expectedResult, IsValidId(movieId));
        }

        [Fact]
        public void CheckIdFormatEntries()
        {
            using (var context = new MovieCupContext())
            {
                var movies = context.Movies.Take(8);
                foreach (var m in movies)
                {
                    Assert.True(IsValidId(m.Id));
                }
            }
        }
    }
}
