﻿using CopaFilmesAPI.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace CopaFilmesAPI.Services
{
    public class MovieSource
    {
        public static readonly string sourceUri = "http://copafilmes.azurewebsites.net/api/filmes";
        private static HttpClient client = new HttpClient();
        private static DateTime lastTimeUpdated = new DateTime(0);

        public static List<Movie> List()
        {
            using (var context = new MovieCupContext())
            {
                var movies = context.Movies.ToList<Movie>();
                return movies;
            }
        }

        public static void Update(List<Movie> movies)
        {
            if (DateTime.Now.Subtract(lastTimeUpdated).TotalDays < 1)
            {
                return;
            }

            lastTimeUpdated = DateTime.Now;

            // This will assure that at least the movies in the request body will be persisted.
            UpdateFromList(movies);
            // So we can call async method without await.
            UpdateFromAPI();
        }

        public static void UpdateFromList(List<Movie> movies)
        {
            using (var context = new MovieCupContext())
            {
                foreach (var m in movies)
                {
                    try
                    {
                        if (context.Movies.Any(x => x.Id == m.Id))
                        {
                            var entry = context.Movies.Find(m.Id);
                            entry.Title = m.Title;
                            entry.Year = m.Year;
                            entry.Rating = m.Rating;
                        }
                        else
                        {
                            context.Movies.Add(new Movie()
                            {
                                Id = m.Id,
                                Title = m.Title,
                                Year = m.Year,
                                Rating = m.Rating
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        throw;
                    }
                }
                context.SaveChanges();
            }
        }

        public static async Task<object> UpdateFromAPI()
        {
            var response = await client.GetAsync(sourceUri);
            string responseBody = await response.Content.ReadAsStringAsync();

            List<Filme> moviesFromApi = JsonConvert.DeserializeObject<List<Filme>>(responseBody);
            
            using (var context = new MovieCupContext())
            {
                foreach (var m in moviesFromApi)
                {
                    try
                    {
                        if (context.Movies.Any(x => x.Id == m.Id))
                        {
                            var entry = context.Movies.Find(m.Id);
                            entry.Title = m.Titulo;
                            entry.Year = m.Ano;
                            entry.Rating = m.Nota;
                        }
                        else
                        {
                            context.Movies.Add(new Movie()
                            {
                                Id = m.Id,
                                Title = m.Titulo,
                                Year = m.Ano,
                                Rating = m.Nota
                            });
                        }
                    }
                    catch(Exception e)
                    {
                        throw;
                    }
                }
                context.SaveChanges();
            }

            return moviesFromApi;
        }

    }
}
