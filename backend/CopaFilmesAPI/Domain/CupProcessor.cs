﻿using CopaFilmesAPI.Data;
using Microsoft.EntityFrameworkCore.Query;
using System.Collections.Generic;
using System.Linq;

namespace CopaFilmesAPI.Domain
{
    public class CupProcessor
    {
        public int Id { get; set; } 
        public Movie FirstPlace { get; set; }
        public Movie SecondPlace { get; set; }

        public List<Movie> Movies;

        public CupProcessor(List<Movie> movies)
        {
            this.Movies = movies;
            Run();

            using (var context = new MovieCupContext())
            {
                var cup = new MovieCup
                {
                    FirstPlaceId = FirstPlace.Id,
                    SecondPlaceId = SecondPlace.Id,
                };
                context.MovieCups.Add(cup);
                context.SaveChanges();

                this.Id = cup.Id;
            }
        }

        public List<Movie> Run()
        {
            Movies = Movies.OrderBy(m => m.Title).ToList();

            List<Match> matches = new List<Match>(7);

            matches.Add(new Match(Movies[0], Movies[7]));
            matches.Add(new Match(Movies[1], Movies[6]));
            matches.Add(new Match(Movies[2], Movies[5]));
            matches.Add(new Match(Movies[3], Movies[4]));

            matches.Add(new Match(matches[0].Winner, matches[1].Winner));
            matches.Add(new Match(matches[2].Winner, matches[3].Winner));

            matches.Add(new Match(matches[4].Winner, matches[5].Winner));
            this.FirstPlace = matches[6].Winner;
            this.SecondPlace = matches[6].Loser;

            return Movies;
        }

        public static MovieCup GetResult(int id)
        {
            using (var context = new MovieCupContext())
            {
                var res = context.MovieCups.First(m => m.Id == id);
                return res;
            }
        }

    }
}
