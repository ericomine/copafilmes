﻿using CopaFilmesAPI.Data;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CopaFilmesAPI.Domain
{
    public class Match
    {
        public Movie Movie1 { get; private set; }
        public Movie Movie2 { get; private set; }
        public Movie Winner { get; private set; }
        public Movie Loser { get; private set; }

        public Match(Movie Movie1, Movie Movie2)
        {
            this.Movie1 = Movie1;
            this.Movie2 = Movie2;
            Run();
        }

        private Movie Run()
        {
            if (Movie1.Rating > Movie2.Rating)
            {
                Winner = Movie1;
                Loser = Movie2;
            }
            else
            {
                Winner = Movie2;
                Loser = Movie1;
            }
            return Winner;
        }
        
    }
}
