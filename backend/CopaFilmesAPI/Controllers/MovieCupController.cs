﻿using CopaFilmesAPI.Data;
using CopaFilmesAPI.Domain;
using CopaFilmesAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace CopaFilmesAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MovieCupController : ControllerBase
    {
        // GET: MovieCup/id
        [HttpGet("{id}")]
        public JsonResult Get(int id)
        {
            MovieCup res = CupProcessor.GetResult(id);
            List<Movie> movies = MovieSource.List();

            var _first = movies.Find(m => m.Id == res.FirstPlaceId);
            var _second = movies.Find(m => m.Id == res.SecondPlaceId);
            if (_first == null || _second == null)
            {
                throw new Exception("Invalid result.");
            }

            List<Object> json = new List<object>();
            json.Add(new
            {
                first = _first,
                second = _second
            });
            return new JsonResult(json);
        }

        // POST: MovieCup
        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody] List<Movie> movies)
        {
            MovieSource.Update(movies);

            var cup = new CupProcessor(movies);

            var result = new HttpResponseMessage(HttpStatusCode.RedirectMethod);
            var domainName = HttpContext.Request.GetDisplayUrl();
            result.Headers.Location = new Uri(domainName + '/' + cup.Id.ToString());
            
            return result;
        }
    }
}
