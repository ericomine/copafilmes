﻿using CopaFilmesAPI.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace CopaFilmesAPI.Data
{
    public class MovieCupContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieCup> MovieCups { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                "Server = (localdb)\\mssqllocaldb; Database = MovieCupData; Trusted_Connection = True;");
        }
    }
}
