﻿using System.Collections.Generic;

namespace CopaFilmesAPI.Data
{
    public class MovieCup
    {
        public int Id { get; set; }
        public string FirstPlaceId { get; set; }
        public string SecondPlaceId { get; set; }
    }
}
