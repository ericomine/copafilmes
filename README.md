# CopaFilmes #

Aplicação simples baseada em:
* ASP.NET Core 3.1
* react com
  * react-bootstrap

## Preview ##

![Demo em resolução PC](./.readme/showcase1.gif)

![Demo em resolução Mobile](./.readme/showcase2.gif)

![Demo em outras resoluções](./.readme/showcase3.gif)

## Compilação e execução ##

Para clonar o repositório é necessário ter o git instalado:
```
$ git clone https://gitlab.com/ericomine/copafilmes.git
```

Para compilar e executar o backend é necessário ter o .NET Core 3.1 instalado:
```
$ cd .\backend\CopaFilmesAPI
$ dotnet build --configuration Release
$ dotnet run --configuration Release

# o servidor vai iniciar em http://localhost:5000
```

Para compilar e executar o frontend é necessário ter o npm instalado:
```
$ cd .\frontend\
# pode ser necessário rodar, caso o npm install --verbose trave;
$ rm package-lock.json
# instala peer deps do react-bootstrap
$ npm install --save jquery@1.9.1 popper.js@1.16.1 typescript@3.8.3
$ npm install --verbose
$ npm start

# o servidor vai iniciar em http://localhost:3000
```

Como o projeto foi pensado para availação em ambiente de desenvolvimento, não foi implementada nenhuma funcionalidade para lidar com CORS. Desse modo é necessário rodar o navegador com esse recurso desabilitado. Para o Chrome, deve-se executar:
```
$ cd [diretório do chrome]
$ chrome --disable-web-security --user-data-dir="C:\temp"
```
Em relação ao backend, também foi desabilitado o uso de redirecionamento HTTPS, de modo a evitar alguns erros com certificados.

## Testes ##

Para executar os testes do frontend:
```
$ cd .\frontend\
$ npm test
```
Você será perguntando se deseja rodar a base toda de testes, ou somente uma parte. Uma parte dos testes depende das respostas entregues pelo backend e só passarão caso o backend esteja em execução. Caso deseje rodar a test suite sem o backend ativo,  comente os testes no arquivo ./src/components/FetchHelpers.api.test.js. Para utilização de integração contínua é necessário modificar esses testes. 

Para executar os testes do backend:
```
$ cd .\backend\CopaFilmesAPI
dotnet test
```

## Funcionamento básico da aplicação ##

1. O frontend requisita da API fornecida a lista de filme e apresenta ao usuário.

2. O usuário seleciona 8 filmes clicando nos cards, o que desbloqueia o botão de envio de dados.

3. Ao clicar no botão, o frontend faz um post no endpoint /MovieCup da API no backend, enviando um objeto JSON com a lista dos filmes.

4. O backend recebe a requisição, persiste os dados, calcula os resultados e responde com HTTP 303 e o recurso a ser buscado pela página de resultados.

5. O frontend redireciona para a página de resultados e requisita do backend o resultado no endpoint /MovieCup/id que foi fornecido em 4. Os dados são apresentados ao usuário.

## Observações ##

A aplicação é responsiva, devido ao uso de `react-bootstrap`.

A implementação foi feita com Cards, porque eu pretendia melhorar a aplicação esteticamente, acessando alguma API pública e recuperar postêres para cada filme, a exibir com `Card.Img={uri}`, porém no tempo disponível só encontrei a API da TMDB que nas chamadas da API não retornava imagens válidas (retornava 404).

![Demo em outras resoluções](./.readme/showcase4.gif)

### TDD ###

* Não utilizei TDD, pois a biblioteca Jest com React não me era familiar e seu uso estava resultando em uma série de conflitos com `import/export` do ES6, chamadas padrão de `fetch()` e configurações do `babel`, de modo que preferi deixar os testes para depois. Esses problemas foram resolvidos:

  1. Utilizando o `react-scripts test` definido pelo próprio `create-react-app` em vez de chamar o `jest` para execução dos testes;

  2. Utilizando a biblioteca `node-fetch` (necessário avaliar se para produção é uma lib estável em relação ao fetch padrão);

### Testes do Frontend ###

* Os testes do frontend englobam alguns tipos diferentes de teste:
  * Unitários (sem interação entre componentes): o componente MovieCard é testado para verificar se a inicialização varia corretamente com os props possíveis.

  * UI (há interação entre componentes): o componente MovieForm é testado para verificar se a quantidade de filmes selecionados desbloqueia o botão de envio de dados, e se a contagem dos filmes está correta. Para esses testes foi usado `enzyme`, que permite interagir com os componentes na página. O componente InfoSection é usa o recurso de snapshots do `jest`.

  * API: as requisições HTTP são ou comparadas a mocks e as respostas recebem verificações básicas. Não utilizei nenhum recurso de mock do `jest` ou `sinon`, o que pode melhorar a qualidade dos testes.

### Testes do Backend ###

* Os testes do frontend englobam um conjunto menor de testes:
  * Integração: verifica se dado um input (mock) recebido do frontend, o resultado calculado é o esperado.

  * Unitários: verifica superficialmente o formato de algumas entradas no DB.

### Utilizando o React ###

Preferi fazer simplificações e manter código verboso em alguns trechos para fazer uso adequado do aspecto reativo do React.

Por exemplo, esse snippet mostra um código que engatilha re-renderização do componente filho devido à modificação do state (que recebe os novos props). 

```
  // src/components/MovieForm.js
  {this.state.movies.map((item, index) => {
    return (
      <Card key={index} ...>
        <MovieCard key={index}
          title={item.titulo}
          year={item.ano}
          selected={item.selected} />
      </Card> );
    })}
```

Em contraponto, esta versão ligeiramente modificada com props  complexos precisa de modificações adicionais que levem o componente filho à re-renderização, de modo que a semântica mais compreensível para um humano não torna o código melhor aqui.

```
  // src/components/MovieForm.js
  {this.state.movies.map((item, index) => {
    return (
      <Card key={index} ...>
        <MovieCard key={index}
          movie={item} />
      </Card> );
    })}
```

Também dei preferência a utilizar React+ES6+ o mais puro possível. Por exemplo, não vi necessidade de utilizar `axios` em detrimento dos requests com fetch() já presentes no ES6+, ou utilizar `redux` ou `mobx` para realizar gerenciamento de estado, já que o projeto é muito pequeno e não há necessidade de erguer o estado na árvore de componentes — de fato, na solução apresentada os estados são passados somente de componente pai para filho.

### Backend ###

O backend é bastante simples e foi desenvolvida com foco em separação de atividades:
* CopaFilmesAPI: projeto contendo os seguintes namespaces:
  * Controllers: contém implementação dos endpoints da API do backend,
  * Data e Migrations: contém implementação do banco de dados/tabelas, as classes criadas aqui são acessadas pelo controller para persistir os dados de entrada e saída;
  * Domain: contém **somente** lógica de regras de negócio,
  * Services: contém chamada à API fornecida que provê uma lista de filmes, isto é feito para preencher a tabela de filmes com os filmes que ainda não foram recebidos via requisição do frontend.
* CopaFilmesAPI.Tests: testes de integração e unitários com xUnit.